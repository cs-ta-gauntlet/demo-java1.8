### Things to note ###
- Class name cannot be Handler.java
- Artifact name must be function
- Must set kubeless as parent
- Must add params as dependency
- Return type must be a String
- Really hard to test as codes cannot be compiled
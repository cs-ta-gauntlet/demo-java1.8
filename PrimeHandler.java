package io.kubeless;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.kubeless.Event;
import io.kubeless.Context;

import java.util.ArrayList;
import java.util.List;

public class PrimeHandler {

    private static final JsonParser parser = new JsonParser();
    private static final Gson gson = new Gson();

    public String prime(Event event, Context context) {
        JsonObject jsonObject = parser.parse(event.Data).getAsJsonObject();
        int num = jsonObject.get("num").getAsInt();
        List<Integer> results = new ArrayList<Integer>();
        int number = 0;
        while(results.size() < num){
            if(isPrime(number)){
                results.add(number);
            }
            number++;
        }
        return gson.toJson(results);
    }

    private static boolean isPrime(int n){
        if(n <= 1){
            return false;
        }
        for(int i = 2; i <= Math.sqrt(n); i++){
            if(n % i == 0){
                return false;
            }
        }
        return true;
    }
}